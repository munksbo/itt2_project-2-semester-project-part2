<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: http://176.23.54.210/");
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Dashboard</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/dash-style.css">
</head>
   <div class="icon-bar">
       <div class="left">
            <a href="../dashboard.php"><i class="fa fa-home"></i></a>
       </div>

       <div class="right">
            <a href="/site/sign-out.php"><i class="fa fa-sign-out"></i></a>
            <a class="active" href="/site/user.php"><i class="fa fa-user"></i></a>
        </div>
</div>
<div class="tool-bar">
    <ul>
        <li><a class="active" href="#home">My info</a></li>
        <li><a href="#news">Add User</a></li>
        <li><a href="#contact">Remove user</a></li>

    </ul>
</div>

</html>
