var baseUrl = 'http://192.168.2.30:5000'

$(document).ready(function(){
    $(".auto-btn").click(() => {
        httpGet(baseUrl+'/pumpauto');
    });

    $(".on-btn").click(() => {
        httpGet(baseUrl+'/pumpon');
    });

    $(".off-btn").click(() => {
        httpGet(baseUrl+'/pumpoff');
    });
});

//HTTP GET
function httpGet(url) {
    return fetch(url)
    .then((response) => {
        var responseClone = response.clone();
        updateLastMessage(response);
        return responseClone;
    })
    .then((responseClone) => responseClone.json())
    .then((responseData) => {
        updateElement(responseData, url);
    })
    // .catch((error) => {
    //     console.error(error)
    //     updateLastMessage(error);
    // });
}

//HTTP PUT
// function httpPut(url, data) {
//     return fetch(url, {
//         method: 'PUT', // 'GET', 'PUT', 'DELETE', etc.
//         body: JSON.stringify(data), // Coordinate the body type with 'Content-Type'
//         headers: new Headers({
//             'Content-Type': 'application/json;charset=UTF-8'
//         }),
//     })
//     .then((responseData) => {
//         httpGet(responseData.url);
//         updateLastMessage(responseData);
//     })
//     .catch((error) => {
//         console.error(error)
//         updateLastMessage(error);
//     });
// }

//OUTPUTS
function updateElement(data, url){
    let element = document.getElementById('status-field-'+url.substr(-2)); //element status-field-A0 etc.
    value = data.value;
    element.innerHTML = value;
    parseInt(value) ? element.style.backgroundColor = "darkolivegreen" : element.style.backgroundColor = "tomato";
}

function updateLastMessage(text)
{
    let time = curTime();
    let li = document.createElement("li");
    li.appendChild(document.createTextNode(`${time} ${text.status} ${text.statusText}`));
    $('#api-log').prepend(li);
}

//CHART CONFIG
function curTime (){
    let now = new Date().toLocaleTimeString('da-DK');
    return now;
}
