<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: http://176.23.54.210/");
    exit;
}
?>

<!DOCTYPE html>
<html lang="en">

<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
<link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>
<script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>

<!-- chart.js CDN link and helper script-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js" integrity="sha256-MZo5XY1Ah7Z2Aui4/alkfeiq3CopMdV/bbkc/Sh41+s=" crossorigin="anonymous"></script>
<script src="utils.js"></script>
<script src="script.js"></script>

<head>
    <meta charset="UTF-8">
    <title>Dashboard</title>
    <link rel="stylesheet" href="./css/dash-style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
   <div class="icon-bar">
       <div class="left">
            <a class="active" href="/dashboard.php"><i class="fa fa-home"></i></a>
       </div>

       <div class="right">
            <a href="/site/sign-out.php"><i class="fa fa-sign-out"></i></a>
            <a href="/site/user.php"><i class="fa fa-user"></i></a>
        </div>
</div>
<div class="tool-bar">
    <ul class="menu">
        <li><a class="active" href="/dashboard.php">Main Side</a></li>
        <li><a href="/site/temp.php">Temperature</a></li>
        <li><a href="/site/mois.php">moisture</a></li>
        <li><a href="/site/alls.php">Joint</a></li>
    </ul>
</div>

<!-- UNDER CONSTRUCTION -->

<div class="container px-5" id="canvas">

        <div class="row cards-container">

            <div class="col-md-4 card-container pb-5">
                <div class="h-100 shadow card col-md-12 mx-auto">
                    <div class="card-body">
                        <h5 class="card-title   text-center">Pump</h5>
                        <div class="card-buttons row mb-3">
                            <button class="btn btn-secondary btn-lg btn-raised buttons" id="on-btn">ON</button>
                            <button class="btn btn-secondary btn-lg btn-raised buttons" id="off-btn">OFF</button>
                            <button class="btn btn-secondary btn-lg btn-raised buttons" id="auto-btn">AUTO</button>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-4 card-container pb-5">
                <div class="h-100 shadow card col-sm-12 log mx-auto">
                    <div class="card-body">
                        <h5 class="card-title">HTTP LOG</h5>
                        <div class="row">
                            <ul id="api-log"</ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</html>
