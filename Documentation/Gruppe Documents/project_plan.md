---
title: '19S ITT2 Project'
subtitle: 'Project plan'
authors: ['Niclas Andersen \<nicas4100@hotmail.com\>', 'Mathias Munksbo Nilesen \<munksbo@hotmail.com\>','Jesper Aggerholm \<Hiddendarknes@gmail.com\>','Daniel Maslygan \<dani7378@edu.eal.dk\>']
date: \today
left-header: \today
right-header: Project plan
skip-toc: false
---



# Background

This is the semester project for ITT2 where we will combine different technologies to go from sensor to cloud. This project will cover the first part of the project, and will match topics that are taught in parallel classes.


# Purpose


The main goal is to have a system where sensor data is send to a cloud server, presented and collected, and data and control signal may be send back also. This is a leaning project and the primary objective is for the students to get a good understanding of the challenges involved in making such a system and to give them hands-on experience with the implementation also.

For simplicity, the goals stated wil evolve around the technical project goals. It must be read using the weekly plan as a complementary source for the *learning* oriented goals.


# Goals

The overall system that is going to be build looks as follows

![project_overview](docs_project_overview.png)

Reading from the left to the right
* Analog input/output and digital input/output: These are sensor of different kinds that we are to implement into the system. Initially we will work with a temperature sensor.
* ATMega328: The embedded system to run the sensor software and to be the interface to the seralconnection to the raspberry pi.
* Raspberry Pi 1 (server): running nginx, mysql database
* Raspberry Pi 2 (API): running flask comunicating with pi 1


Project deliveries are:
* A system reading and writing data to and from the sensors/actuators
* A REST API exposing the values of the sensors and option of setting and applying the actuator values.
* Docmentation of all parts of the solution
* Regular meeting with project stakeholders.
* Final evaluation


# Schedule

The project is running from week 17 until end of june 2019


# Organization

Jesper Tinus Aggerholm
- Jespertinusaggerholm@gmail.com
- Jesp9795@edu.eal.dk
- Hiddendarknes@gmail.com(Main email)

Mobile: 42333990


Mathias Munksbo Nilesen
- munksbo@hotmail.com 

Mobile: 31441665

Niclas Orlung Baisgaard Andersen
- nicas4100@hotmail.com 
- nicl0575@edu.eal.dk

Mobile: 53697560

Daniel Maslygan
- dani7378@edu.eal.dk

Mobile: 42635149

# Budget and resources

No monetary resources are expected. In terms of manpower, only the people in the project group are expected to contrbute,
If there where to be any expenses, then the cost will be split equely between the group members.

# Risk assessment

[The risks involved in the project is evaluated, and the major issues will be listed. The plan will include the actions taken as part of the project design to handle the risk or the actions planned should a given risk materialize.]


* Time management 
* Too high ambitions
* Teamwork
* Lack of documentation
* Lack of knowledge

* Poor communication
* Poor project management
* Lack of knowledge 
* Delibret actions(sabotage??)
* Not participating

* Lack of motivation 
* Electrical failures
* Horribel planning
* Lack of communication
* Lack of imagination

How to overcome these issues.

Knowledge.

- Seek knowledge / help from class mates.
- Group preasure?? 
- Be honest with the goals, never hide your progress! 
- Reduce scope of the tasks/project

Documentation.

- Lack of teacher docs
- Lack of student docs
- Who does what in the project, take notes! so you can identify the issues.

Planning. 

- Make sure to keep group meetings, a regualr thing. So all are on the same page!
- Never leave issues unresolved! make prioties

Motivation. 

- If you lack the motivation, talk with the team. find a solution!



# Stakeholders

* Teachers
* (Future investors)
* Friends
* Classmates 

# Communication

The group will be having a daily meeting, from 8.30 to discuss the tasks of the days.
we will be communicating through Messenger, phone, email, Gitlab, Discord.

Weekly communication with the teachers in a 10 minute meeting.

in the meeting we will be covering.
* agenda
* showcase completed delivables
* current issues (opened and closed)

# Perspectives

This project will serve as a template for other similar project, in 3rd semester.

It can also serve as an example of the students abilities in their portfolio, when applying for jobs or internships.


# Evaluation

This project will be evaluated with a user satisfaction test.

# References

[Include any relevant references to legal documents, literature, books, home pages or other material that is relevant for the reader.]

None at this time
