from flask import Flask, request
from flask_restful import reqparse, abort, Api, Resource
from flask_cors import CORS, cross_origin
#from atmega import *
#from organize import *

app = Flask(__name__)
CORS(app, resources=r'/*')
api = Api(app)

status = 0

@api.resource("/")
class url_index(Resource):
    def get(self):
        returnMessage = {"message": "O.K" }
        print("det virker")
        return returnMessage

@api.resource("/dashboard.php/pump_ON")
class url_index_pump_on(Resource):
    def get(self):
        returnMessage = {"message": "Pump ON" }
        #send('Pump_ON')
        print("pump off")
        return returnMessage

@api.resource("/dashboard.php/pump_OFF")
class url_index_pump_off(Resource):
    def get(self):
        returnMessage = {"message": "Pump OFF" }
        #send('Pump_OFF')
        print("pump off")
        return returnMessage

@api.resource("/dashboard.php/auto")
class url_index_auto(Resource):
    def get(self):
        returnMessage = {"message": "auto enabled" }
        #send('auto')
        print("auto enbled")
        return returnMessage

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
