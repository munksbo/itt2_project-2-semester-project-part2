!/usr/bin/env python3

import serial
import time

def send(message):
    with serial.Serial('/dev/ttyS0', 9600, timeout=1) as ser:
        try:
            ser.write( message+"\n".encode() )
            return
        except SerialException as e:
            print(e)

def retrieve():
    with serial.Serial('/dev/ttyS0', 9600, timeout=1) as ser:
        try:
            reply = ser.readline().decode()
            return reply
        except SerialException as e:
            print(e)
    # test = "light@100,temp@20 ,moisture@35"
    # return test
