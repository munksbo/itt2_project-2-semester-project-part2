from flask import Flask, request
#from flask_restfull import reqparse, abort, Api, Resource
#from flask_cors import CORS, cross_origin
from atmega import *
from organize import *

app = Flask(__name__)

@app.route('/dashboard.php', methods = ['GET', 'POST'])
def get():
    if request.method == 'GET':
        print(session['loggedin'])

if __name__ == '__main__':
    app.run(host='127.0.0.1', debug=True)
