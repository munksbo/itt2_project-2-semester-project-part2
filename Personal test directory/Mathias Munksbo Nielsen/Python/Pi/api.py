import mysql.connector
from mysql.connector import Error
try:
    mySQLconnection = mysql.connector.connect(host='176.23.54.210',database='munksbo',user='admin',password='password')
    sql_select_Query = "select * from users"
    cursor = mySQLconnection.cursor()
    cursor.execute(sql_select_Query)
    records = cursor.fetchall()
    for row in records:
        print("Id = ", row[0], )
        print("username = ", row[1])
        print("password  = ", row[2])
        print("Date   = ", row[3], "\n")
        cursor.close()

except Error as e :
    print ("Error while connecting to MySQL", e)

finally:
    if(mySQLconnection.is_connected()):
        mySQLconnection.close()
        print("MySQL connection is closed")
